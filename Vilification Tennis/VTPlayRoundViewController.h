//
//  VTPlayRoundViewController.h
//  Vilification Tennis
//
//  Created by Jason Edstrom on 3/9/15.
//  Copyright (c) 2015 Jason Edstrom. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VTGameTimerView;
@class VTGameTimerProgress;
@class VTAdvanceButton;

@interface VTPlayRoundViewController : UIViewController
@property (weak, nonatomic) VTGameTimerView *gameTimerView;
//@property (weak, nonatomic) IBOutlet VTGameTimerView *gameTimerView;
//@property (weak, nonatomic) IBOutlet VTGameTimerProgress *gameTimerProgressView;


@end
