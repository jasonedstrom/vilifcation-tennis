//
//  VTRoundSetupViewController.h
//  Vilification Tennis
//
//  Created by Jason Edstrom on 2/19/15.
//  Copyright (c) 2015 Jason Edstrom. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VTAdvanceButton;
@class VTGameTimerView;

@interface VTRoundSetupViewController : UIViewController

@property (weak, nonatomic) IBOutlet VTAdvanceButton *startRoundButton;



@end
