//
//  VTUIElements.m
//  Vilification Tennis Home Game
//
//  Created by Jason Edstrom on 6/14/15.
//  Copyright (c) 2015 CompanyName. All rights reserved.
//
//  Generated by PaintCode (www.paintcodeapp.com)
//

#import "VTUIElements.h"


@implementation VTUIElements

#pragma mark Cache

static UIColor* _actionColor = nil;
static UIColor* _textOptionColor = nil;
static UIColor* _clickedColor = nil;
static UIColor* _titleBarColor = nil;

#pragma mark Initialization

+ (void)initialize
{
    // Colors Initialization
    _actionColor = [UIColor colorWithRed: 0.298 green: 0.208 blue: 0.498 alpha: 1];
    _textOptionColor = [UIColor colorWithRed: 1 green: 1 blue: 1 alpha: 1];
    _clickedColor = [UIColor colorWithRed: 0.502 green: 0.502 blue: 0.502 alpha: 0.502];
    _titleBarColor = [UIColor colorWithRed: 0.255 green: 0.463 blue: 0.282 alpha: 1];

}

#pragma mark Colors

+ (UIColor*)actionColor { return _actionColor; }
+ (UIColor*)textOptionColor { return _textOptionColor; }
+ (UIColor*)clickedColor { return _clickedColor; }
+ (UIColor*)titleBarColor { return _titleBarColor; }

#pragma mark Drawing Methods

+ (void)drawActionButtonWithFrame: (CGRect)frame
{

    //// Button Shape Drawing
    UIBezierPath* buttonShapePath = UIBezierPath.bezierPath;
    [buttonShapePath moveToPoint: CGPointMake(CGRectGetMinX(frame) + 144, CGRectGetMinY(frame))];
    [buttonShapePath addLineToPoint: CGPointMake(CGRectGetMaxX(frame) - 14.93, CGRectGetMinY(frame))];
    [buttonShapePath addCurveToPoint: CGPointMake(CGRectGetMaxX(frame) - 4.37, CGRectGetMinY(frame) + 6) controlPoint1: CGPointMake(CGRectGetMaxX(frame) - 11.11, CGRectGetMinY(frame)) controlPoint2: CGPointMake(CGRectGetMaxX(frame) - 7.29, CGRectGetMinY(frame) + 2)];
    [buttonShapePath addCurveToPoint: CGPointMake(CGRectGetMaxX(frame) - 4.37, CGRectGetMaxY(frame) - 6) controlPoint1: CGPointMake(CGRectGetMaxX(frame) + 1.46, CGRectGetMinY(frame) + 14.01) controlPoint2: CGPointMake(CGRectGetMaxX(frame) + 1.46, CGRectGetMaxY(frame) - 14.01)];
    [buttonShapePath addCurveToPoint: CGPointMake(CGRectGetMaxX(frame) - 14.93, CGRectGetMaxY(frame)) controlPoint1: CGPointMake(CGRectGetMaxX(frame) - 7.29, CGRectGetMaxY(frame) - 2) controlPoint2: CGPointMake(CGRectGetMaxX(frame) - 11.11, CGRectGetMaxY(frame))];
    [buttonShapePath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 143, CGRectGetMaxY(frame))];
    [buttonShapePath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 14.93, CGRectGetMaxY(frame))];
    [buttonShapePath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 4.37, CGRectGetMaxY(frame) - 6) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 11.11, CGRectGetMaxY(frame)) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 7.29, CGRectGetMaxY(frame) - 2)];
    [buttonShapePath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 4.37, CGRectGetMinY(frame) + 6) controlPoint1: CGPointMake(CGRectGetMinX(frame) - 1.46, CGRectGetMaxY(frame) - 14.01) controlPoint2: CGPointMake(CGRectGetMinX(frame) - 1.46, CGRectGetMinY(frame) + 14.01)];
    [buttonShapePath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 5.01, CGRectGetMinY(frame) + 5.18) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 4.58, CGRectGetMinY(frame) + 5.72) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 4.79, CGRectGetMinY(frame) + 5.44)];
    [buttonShapePath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 14.93, CGRectGetMinY(frame)) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 7.83, CGRectGetMinY(frame) + 1.73) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 11.38, CGRectGetMinY(frame))];
    [buttonShapePath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 144, CGRectGetMinY(frame))];
    [buttonShapePath closePath];
    [VTUIElements.actionColor setFill];
    [buttonShapePath fill];
}

+ (void)drawOptionButtonWithFrame: (CGRect)frame
{

    //// Button Shape Drawing
    UIBezierPath* buttonShapePath = UIBezierPath.bezierPath;
    [buttonShapePath moveToPoint: CGPointMake(CGRectGetMinX(frame) + 266.63, CGRectGetMinY(frame) + 1.5)];
    [buttonShapePath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 277.14, CGRectGetMinY(frame) + 7.36) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 270.43, CGRectGetMinY(frame) + 1.5) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 274.24, CGRectGetMinY(frame) + 3.45)];
    [buttonShapePath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 277.14, CGRectGetMinY(frame) + 35.64) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 282.95, CGRectGetMinY(frame) + 15.17) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 282.95, CGRectGetMinY(frame) + 27.83)];
    [buttonShapePath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 266.63, CGRectGetMinY(frame) + 41.5) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 274.24, CGRectGetMinY(frame) + 39.55) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 270.43, CGRectGetMinY(frame) + 41.5)];
    [buttonShapePath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 17.37, CGRectGetMinY(frame) + 41.5)];
    [buttonShapePath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 6.86, CGRectGetMinY(frame) + 35.64) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 13.57, CGRectGetMinY(frame) + 41.5) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 9.76, CGRectGetMinY(frame) + 39.55)];
    [buttonShapePath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 6.86, CGRectGetMinY(frame) + 7.36) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 1.05, CGRectGetMinY(frame) + 27.83) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 1.05, CGRectGetMinY(frame) + 15.17)];
    [buttonShapePath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 7.49, CGRectGetMinY(frame) + 6.55) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 7.06, CGRectGetMinY(frame) + 7.08) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 7.28, CGRectGetMinY(frame) + 6.81)];
    [buttonShapePath addCurveToPoint: CGPointMake(CGRectGetMinX(frame) + 17.37, CGRectGetMinY(frame) + 1.5) controlPoint1: CGPointMake(CGRectGetMinX(frame) + 10.3, CGRectGetMinY(frame) + 3.18) controlPoint2: CGPointMake(CGRectGetMinX(frame) + 13.84, CGRectGetMinY(frame) + 1.5)];
    [buttonShapePath addLineToPoint: CGPointMake(CGRectGetMinX(frame) + 266.63, CGRectGetMinY(frame) + 1.5)];
    [buttonShapePath closePath];
    [VTUIElements.textOptionColor setFill];
    [buttonShapePath fill];
    [UIColor.blackColor setStroke];
    buttonShapePath.lineWidth = 1;
    [buttonShapePath stroke];
}

+ (void)drawGameTimerWithRectangle: (CGRect)rectangle
{
    //// General Declarations
    CGContextRef context = UIGraphicsGetCurrentContext();


    //// Shadow Declarations
    NSShadow* shadow = [[NSShadow alloc] init];
    [shadow setShadowColor: [UIColor.blackColor colorWithAlphaComponent: 0.91]];
    [shadow setShadowOffset: CGSizeMake(0.1, -0.1)];
    [shadow setShadowBlurRadius: 5];
    NSShadow* shadow2 = [[NSShadow alloc] init];
    [shadow2 setShadowColor: UIColor.blackColor];
    [shadow2 setShadowOffset: CGSizeMake(0.1, -0.1)];
    [shadow2 setShadowBlurRadius: 36];

    //// Variable Declarations
    CGFloat progress = 0.01;
    CGFloat progressBar2 = progress * 360;
    NSString* timerLabel = @"2 : 30";
    CGFloat expression = 56 - (411 - rectangle.size.height) / rectangle.size.height * 56;

    //// Frames
    CGRect frame = CGRectMake(rectangle.origin.x, rectangle.origin.y, rectangle.size.width, rectangle.size.height);

    //// Subframes
    CGRect innerTimer = CGRectMake(CGRectGetMinX(frame) + floor(CGRectGetWidth(frame) * 0.02381 + 0.5), CGRectGetMinY(frame) + floor(CGRectGetHeight(frame) * 0.02381 + 0.5), floor(CGRectGetWidth(frame) * 0.97857 + 0.5) - floor(CGRectGetWidth(frame) * 0.02381 + 0.5), floor(CGRectGetHeight(frame) * 0.97619 + 0.5) - floor(CGRectGetHeight(frame) * 0.02381 + 0.5));


    //// Oval Drawing
    UIBezierPath* ovalPath = [UIBezierPath bezierPathWithOvalInRect: CGRectMake(CGRectGetMinX(frame) + floor(CGRectGetWidth(frame) * 0.01905 + 0.5), CGRectGetMinY(frame) + floor(CGRectGetHeight(frame) * 0.01905 + 0.5), floor(CGRectGetWidth(frame) * 0.98333 + 0.5) - floor(CGRectGetWidth(frame) * 0.01905 + 0.5), floor(CGRectGetHeight(frame) * 0.98333 + 0.5) - floor(CGRectGetHeight(frame) * 0.01905 + 0.5))];
    [UIColor.grayColor setFill];
    [ovalPath fill];


    //// Progress Bar Drawing
    CGRect progressBarRect = CGRectMake(CGRectGetMinX(frame) + floor(CGRectGetWidth(frame) * 0.01905 + 0.5), CGRectGetMinY(frame) + floor(CGRectGetHeight(frame) * 0.01786) + 0.5, floor(CGRectGetWidth(frame) * 0.98333 + 0.5) - floor(CGRectGetWidth(frame) * 0.01905 + 0.5), floor(CGRectGetHeight(frame) * 0.98214) - floor(CGRectGetHeight(frame) * 0.01786));
    UIBezierPath* progressBarPath = UIBezierPath.bezierPath;
    [progressBarPath addArcWithCenter: CGPointMake(CGRectGetMidX(progressBarRect), CGRectGetMidY(progressBarRect)) radius: CGRectGetWidth(progressBarRect) / 2 startAngle: -90 * M_PI/180 endAngle: -(progressBar2 + 90) * M_PI/180 clockwise: YES];
    [progressBarPath addLineToPoint: CGPointMake(CGRectGetMidX(progressBarRect), CGRectGetMidY(progressBarRect))];
    [progressBarPath closePath];

    [VTUIElements.titleBarColor setFill];
    [progressBarPath fill];
    [VTUIElements.titleBarColor setStroke];
    progressBarPath.lineWidth = 3;
    [progressBarPath stroke];


    //// Inner + Timer
    {
        //// Oval 2 Drawing
        UIBezierPath* oval2Path = [UIBezierPath bezierPathWithOvalInRect: CGRectMake(CGRectGetMinX(innerTimer) + floor(CGRectGetWidth(innerTimer) * 0.00000 + 0.5), CGRectGetMinY(innerTimer) + floor(CGRectGetHeight(innerTimer) * 0.00000 + 0.5), floor(CGRectGetWidth(innerTimer) * 1.00000 + 0.5) - floor(CGRectGetWidth(innerTimer) * 0.00000 + 0.5), floor(CGRectGetHeight(innerTimer) * 1.00000 + 0.5) - floor(CGRectGetHeight(innerTimer) * 0.00000 + 0.5))];
        CGContextSaveGState(context);
        CGContextSetShadowWithColor(context, shadow.shadowOffset, shadow.shadowBlurRadius, [shadow.shadowColor CGColor]);
        [UIColor.whiteColor setStroke];
        oval2Path.lineWidth = 10;
        [oval2Path stroke];
        CGContextRestoreGState(context);


        //// Inner Drawing
        UIBezierPath* innerPath = [UIBezierPath bezierPathWithOvalInRect: CGRectMake(CGRectGetMinX(innerTimer) + floor(CGRectGetWidth(innerTimer) * 0.12791 + 0.21) + 0.29, CGRectGetMinY(innerTimer) + floor(CGRectGetHeight(innerTimer) * 0.12575 - 0.2) + 0.7, floor(CGRectGetWidth(innerTimer) * 0.87604 + 0.21) - floor(CGRectGetWidth(innerTimer) * 0.12791 + 0.21), floor(CGRectGetHeight(innerTimer) * 0.87575 - 0.2) - floor(CGRectGetHeight(innerTimer) * 0.12575 - 0.2))];
        CGContextSaveGState(context);
        CGContextSetShadowWithColor(context, shadow2.shadowOffset, shadow2.shadowBlurRadius, [shadow2.shadowColor CGColor]);
        [VTUIElements.actionColor setFill];
        [innerPath fill];
        CGContextRestoreGState(context);

        [VTUIElements.actionColor setStroke];
        innerPath.lineWidth = 5;
        [innerPath stroke];


        //// Oval 3 Drawing
        UIBezierPath* oval3Path = [UIBezierPath bezierPathWithOvalInRect: CGRectMake(CGRectGetMinX(innerTimer) + floor(CGRectGetWidth(innerTimer) * 0.12469 + 0.5), CGRectGetMinY(innerTimer) + floor(CGRectGetHeight(innerTimer) * 0.12500 + 0.5), floor(CGRectGetWidth(innerTimer) * 0.87282 + 0.5) - floor(CGRectGetWidth(innerTimer) * 0.12469 + 0.5), floor(CGRectGetHeight(innerTimer) * 0.87500 + 0.5) - floor(CGRectGetHeight(innerTimer) * 0.12500 + 0.5))];
        [UIColor.whiteColor setStroke];
        oval3Path.lineWidth = 10;
        [oval3Path stroke];
    }
}

@end
