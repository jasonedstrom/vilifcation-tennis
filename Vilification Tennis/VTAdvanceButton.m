//
//  VTAdvanceButton.m
//  Vilification Tennis
//
//  Created by Jason Edstrom on 3/9/15.
//  Copyright (c) 2015 Jason Edstrom. All rights reserved.
//

#import "VTAdvanceButton.h"
#import "VTUIElements.h"
@interface VTAdvanceButton ()




@end

@implementation VTAdvanceButton


// Only override drawRect: if you perform custom drawing.                                                                                                           ®
// An empty implementation adversely affects performance during animation.




- (void)drawRect:(CGRect)rect {

    [VTUIElements drawActionButtonWithFrame:rect];

}






@end
