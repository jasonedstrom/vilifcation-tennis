//
//  VTGameTimerProgress.m
//  Vilification Tennis
//
//  Created by Jason Edstrom on 6/14/15.
//  Copyright (c) 2015 Jason Edstrom. All rights reserved.
//

#import "VTGameTimerProgress.h"
#import "VTUIElements.h"

@implementation VTGameTimerProgress


-(void)drawRect:(CGRect)rect{
    
    [VTUIElements drawGameTimerWithRectangle:rect];
}
@end
