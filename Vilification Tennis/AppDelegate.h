//
//  AppDelegate.h
//  Vilification Tennis
//
//  Created by Jason Edstrom on 2/19/15.
//  Copyright (c) 2015 Jason Edstrom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

