//
//  VTGameTimerView.m
//  Vilification Tennis
//
//  Created by Jason Edstrom on 3/11/15.
//  Copyright (c) 2015 Jason Edstrom. All rights reserved.
//

#import "VTGameTimerView.h"
#import "VTUIElements.h"

#define kSecondsForCompleteUpdate  3.0
#define kUpdateInterval 1.0

@interface VTGameTimerView  ()

     @property (nonatomic) CGRect cgRect;

@end

@implementation VTGameTimerView

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code

    [VTUIElements drawGameTimerWithRectangle:rect];
    //[VTUIElements drawGameTimerWithProgress:.50f timerLabel:@"1:30" rectangle:rect];
    //[VTUIElements drawGameTimerWithProgress:self.progress timerLabel:self.timerLabel rectangle:rect];
    self.cgRect = rect;


}

- (void)updateTimer{
//    [VTUIElements drawGameTimerWithProgress:self.progress
//                                 timerLabel:self.timerLabel
//                                  rectangle:self.cgRect];

    
}


@end
