//
//  VTGameTimerView.h
//  Vilification Tennis
//
//  Created by Jason Edstrom on 3/11/15.
//  Copyright (c) 2015 Jason Edstrom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VTGameTimerView : UIView

@property (nonatomic) CGFloat progress;
@property (weak, nonatomic) NSString *timerLabel;

-(void) updateTimer;

@end
