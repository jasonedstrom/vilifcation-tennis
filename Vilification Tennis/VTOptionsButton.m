//
//  VTOptionsButton.m
//  Vilification Tennis
//
//  Created by Jason Edstrom on 6/9/15.
//  Copyright (c) 2015 Jason Edstrom. All rights reserved.
//

#import "VTOptionsButton.h"
#import "VTUIElements.h"

@implementation VTOptionsButton


- (void) drawRect:(CGRect)rect
{
    [VTUIElements drawOptionButtonWithFrame:rect];
}

@end
