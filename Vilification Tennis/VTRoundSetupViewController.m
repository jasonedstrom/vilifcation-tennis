//
//  VTRoundSetupViewController.m
//  Vilification Tennis
//
//  Created by Jason Edstrom on 2/19/15.
//  Copyright (c) 2015 Jason Edstrom. All rights reserved.
//

#import "VTRoundSetupViewController.h"
#import <Crashlytics/Crashlytics.h>
#import "VTAdvanceButton.h"
#import "VTPlayRoundViewController.h"
#import "VTGameTimerView.h"

@interface VTRoundSetupViewController ()

@property (strong, nonatomic) NSArray *wordArray;


@end

@implementation VTRoundSetupViewController




- (void)viewDidLoad
{

    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.wordArray = @[@"Word 1",
                       @"Word 2",
                       @"Word 3",
                       @"Word 4",
                       @"Word 5"];
    //self.topicLabel.text = self.wordArray[0];
    

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)InstaCrashPressed:(id)sender {
    
    [[Crashlytics sharedInstance] crash];
}


- (IBAction)startRoundPressed:(id)sender {
    //[self.startRoundButton setHighlighted:YES];
    NSLog(@"Click Click, MuthaFucka!!");
    //[self.startRoundButton setHighlighted:NO];
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"playRound"]) {
        VTPlayRoundViewController *playRoundViewController = (VTPlayRoundViewController *) segue.destinationViewController;
    }
}


@end
