//
//  VTPlayRoundViewController.m
//  Vilification Tennis
//
//  Created by Jason Edstrom on 3/9/15.
//  Copyright (c) 2015 Jason Edstrom. All rights reserved.
//

#import "VTPlayRoundViewController.h"
#import "VTGameTimerView.h"

@interface VTPlayRoundViewController ()
@property (weak, nonatomic) IBOutlet UILabel *selectedTopicLbl;
//@property int timeTick;
@end

@implementation VTPlayRoundViewController
int timeTick = 180;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
    NSArray *topics = @[@"Word 1", @"Word 2", @"Word 3"];
    self.selectedTopicLbl.text = topics[1];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)  startTimer{
      NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                        target:self
                                                      selector:@selector(incrementTimer)
                                                      userInfo:nil
                                                       repeats:YES];
}

- (void) incrementTimer{
    timeTick--;
    NSString *timeString = [[NSString alloc]
                                      initWithFormat:@"%d", timeTick];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
